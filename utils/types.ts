export interface State {
  movies: Movie[];
  sort: Sort;
}

export interface Movie {
  title: string;
  year: number;
  actor: string;
  img?: string;
  director: string;
}

export interface Sort {
  name: string;
  code: string;
  key: keyof Movie;
}

export interface Base64 {
  img: string;
  movie: Movie;
}
