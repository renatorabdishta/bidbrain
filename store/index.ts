import { Base64, Movie, Sort, State } from "../utils/types";

const movies = require("static/movies.json").movies;

const sortArrayByKey = <T>(arr: T[], key: keyof T) => {
  return arr.sort(function (a, b) {
    if (a[key] < b[key]) {
      return 1;
    } else if (a[key] > b[key]) {
      return -1;
    } else {
      return 0;
    }
  });
};

export const state = () =>
  ({
    movies,
    sort: {},
  } as State);

export const getters = {
  getPaginatedMovies:
    (state: State) =>
    (page_number: number = 1, page_size: number = 10) => {
      const movies = state.movies.slice(
        (page_number - 1) * page_size,
        page_number * page_size
      );
      console.log(movies);
      if (Object.keys(state.sort).length) {
        return sortArrayByKey<Movie>(movies, state.sort.key);
      } else {
        return movies.sort((movieA, movieB) => movieB.year - movieA.year);
      }
    },
  getFilteredMovies: (state: State) => (query: string) => {
    if (!query) {
      return state.movies;
    }
    const movies = state.movies.filter((movie: Movie) =>
      movie.title.toLowerCase().includes(query)
    );

    if (Object.keys(state.sort).length) {
      return sortArrayByKey<Movie>(movies, state.sort.key);
    } else {
      return movies.sort((movieA, movieB) => movieB.year - movieA.year);
    }
  },

  getSortedMovies(state: State) {
    const cpMovies = [...state.movies];
    return sortArrayByKey<Movie>(cpMovies, state.sort.key);
  },
};

export const mutations = {
  addSort: (state: State, payload: Sort) => {
    state.sort = payload;
  },
  addImage: (state: State, payload: Base64) => {
    state.movies = state.movies.map((movie) => {
      if (movie.title === payload.movie.title) {
        movie.img = payload.img;
      }
      return movie;
    });
  },
};

export const actions = {
  changeSort: (context: any, payload: Sort) => {
    return context.commit("addSort", payload);
  },
  addImageToCard: (context: any, payload: Base64) => {
    return context.commit("addImage", payload);
  },
};
